package az.ingress.one2one.repository;

import az.ingress.one2one.model.Employee;
import az.ingress.one2one.model.EmployeeDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeDetailRepository extends JpaRepository<EmployeeDetail,Integer> {
}
