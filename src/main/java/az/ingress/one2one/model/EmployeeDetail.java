package az.ingress.one2one.model;

import jakarta.persistence.*;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "employee_detail")
public class EmployeeDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "age")
    private Integer age;

    @Column(name = "city")
    private String city;

    //Unidirectional
    //@OneToOne(cascade = CascadeType.ALL)
    //@JoinColumn(name = "emp_id", referencedColumnName = "id")

    //Bidirectional
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "emp_id", referencedColumnName = "id")
    @ToString.Exclude
    private Employee employee;


}
