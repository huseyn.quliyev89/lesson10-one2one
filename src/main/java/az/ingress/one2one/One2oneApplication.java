package az.ingress.one2one;

import az.ingress.one2one.model.Employee;
import az.ingress.one2one.model.EmployeeDetail;
import az.ingress.one2one.repository.EmployeeDetailRepository;
import az.ingress.one2one.repository.EmployeeRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Optional;

@SpringBootApplication
public class One2oneApplication implements CommandLineRunner {

	private final EmployeeDetailRepository employeeDetailRepository;

	public One2oneApplication(EmployeeDetailRepository employeeDetailRepository) {
		this.employeeDetailRepository = employeeDetailRepository;
	}


	public static void main(String[] args) {
		SpringApplication.run(One2oneApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//  --Unidirectional--
//		Employee employee = Employee.builder()
//				.name("Huseyn")
//				.surname("Guliyev")
//				.build();
//
//		EmployeeDetail employeeDetail = EmployeeDetail.builder()
//				.age(34)
//				.city("Baku")
//				.employee(employee)
//				.build();
//
//		employeeDetailRepository.save(employeeDetail);
// --Unidirectional--

//		  --Bidirectional--
		Employee employee = Employee.builder()
				.name("Huseyn")
				.surname("Guliyev")
				.build();

		EmployeeDetail employeeDetail = EmployeeDetail.builder()
				.age(34)
				.city("Baku")
				.employee(employee)
				.build();
		employee.setEmployeeDetail(employeeDetail);

		employeeDetailRepository.save(employeeDetail);
// --Bidirectional--

		EmployeeDetail employeeDetail2 = employeeDetailRepository.findById(1).get();
System.out.println(employeeDetail2);
System.out.println(employeeDetail2.getEmployee());








	}
}
